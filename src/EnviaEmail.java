import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import java.sql.Date;

public class EnviaEmail {

	// Esse codigo envia e-mail para o servidor do GMAIL
	// Email de teste: newlandteste@gmail.com Senha:newland123456
	public static void Envia(String placa, String modelo, String ano)
			throws EmailException {
		System.out.println("iniciando envio de Emails");
		SimpleEmail emailsimples = new SimpleEmail();
		// SMTP do Email
		emailsimples.setHostName("smtp.gmail.com");
		// Porta do Envio
		emailsimples.setSmtpPort(465);
		// Dados de quem est� enviando
		emailsimples.setFrom("newlandteste@gmail.com",
				"Newland - Controle de dados ");
		// Emails dos receptores
		//emailsimples.addTo("newlandteste@gmail.com", "NewlandTeste");
		emailsimples.addTo("carlosdavilira@gmail.com", "Davi");
		// Assunto do Email
		emailsimples.setSubject("Uma Nova Avalia��o foi Registrada");
		// Mensagem do Email
		emailsimples
				.setMsg("Foi Registrado uma Nova Avalia��o: \r\n \r\n Placa:"
						+ placa
						+ "\r\n Modelo:"
						+ modelo
						+ "\r\n Ano:"
						+ ano
						+ "\r\n \r\n \r\n \r\n \r\n Software de Avalia��o \r\n Desenvolvido por: Carlos David Lira (85)8756-6040 \r\n E-mail:carlosdavilira@gmail.com");
		emailsimples.setSSL(true);
		// Login e Senha da conta que ser� usada
		emailsimples.setAuthentication("newlandteste@gmail.com",
				"newland123456");
		// Envia o email
		emailsimples.send();
		System.out.println("Mensagem enviada com sucesso!");
	}
}
